import Foundation

public struct ShiftsGroupRequestDTO: Codable {
    public let validFrom: Date
    public let category: String
    public let location: String
    
    public init(validFrom: Date, category: String, location: String) {
        self.validFrom = validFrom
        self.category = category
        self.location = location
    }
}
