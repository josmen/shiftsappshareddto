import Foundation

public struct UserResponseDTO: Codable {
    public var id: UUID
    public var username: String
    public var isAdmin: Bool
    
    public init(id: UUID, username: String, isAdmin: Bool) {
        self.id = id
        self.username = username
        self.isAdmin = isAdmin
    }
}
