import Foundation

public struct ShiftRequestDTO: Codable {
    public let name: String
    public let start: String
    public let end: String
    public let saturation: Double?
    public let shiftsGroupID: UUID
    
    public init(name: String, start: String, end: String, saturation: Double = 0, shiftsGroupID: UUID) {
        self.name = name
        self.start = start
        self.end = end
        self.saturation = saturation
        self.shiftsGroupID = shiftsGroupID
    }
}
