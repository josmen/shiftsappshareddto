import Foundation

public struct ShiftsGroupResponseDTO: Codable {
    public let id: UUID
    public let validFrom: Date
    public let category: String
    public let location: String
    public var shifts: [ShiftResponseDTO]?
    
    public init(id: UUID, validFrom: Date, category: String, location: String, shifts: [ShiftResponseDTO]? = nil) {
        self.id = id
        self.validFrom = validFrom
        self.category = category
        self.location = location
        self.shifts = shifts
    }
}
