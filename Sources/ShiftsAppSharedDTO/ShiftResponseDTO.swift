import Foundation

public struct ShiftResponseDTO: Codable {
    public var id: UUID
    public var name: String
    public var start: String
    public var end: String
    public var saturation: Double
    public var shiftsGroupID: UUID
    
    public init(id: UUID, name: String, start: String, end: String, saturation: Double = 0, shiftsGroupID: UUID) {
        self.id = id
        self.name = name
        self.start = start
        self.end = end
        self.saturation = saturation
        self.shiftsGroupID = shiftsGroupID
    }
}
