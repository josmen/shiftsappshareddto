//
//  File.swift
//  
//
//  Created by Jose Antonio Mendoza on 29/10/23.
//

import Foundation

public struct MetadataResponseDTO: Codable {
    public var updatedAt: Date
    public var shiftsGroups: [ShiftsGroupResponseDTO]
    
    public init(updatedAt: Date, shiftsGroups: [ShiftsGroupResponseDTO]) {
        self.updatedAt = updatedAt
        self.shiftsGroups = shiftsGroups
    }
}
